﻿<#
 # This script backups all your important data to a network share. THIS IS ONLY A TEMPLATE FILE
 #
 # Steps done by this script:
 # 1. Greets user  
 # 2. Copy data with Robocopy
 #
 #
 # Author: Robert Johner
 # Date: 23.02.2018
 # Version: 1.0
 #>

# VARIABLES
# network location
$defaultServerName = "XXX";
$defaultBackupShareName = "XXX";
$defaultBackupFolderName = "XXX";
$defaultNetworkBackupPath = "\\" + $defaultServerName + "\" + $defaultBackupShareName + "\" + $defaultBackupFolderName;
# sources
$sources =  "C:\Temp",
            ($ENV:UserProfile + "\Pictures"),
            ($ENV:UserProfile + "\Music");
# destinations
$destinations = ($defaultNetworkBackupPath + "\Temp"),
                ($defaultNetworkBackupPath + "\Profile\Pictures"),
                ($defaultNetworkBackupPath + "\Profile\Music");
# log
$defaultLogFileName = "backup_XXX_"+ $(get-date -f dd-MM-yyyy_HH_mm_ss) +".log";
$defaultLocalLogPath = "C:\backupInProgress\" + $defaultLogFileName;
$defaultNetworkLogPath = $defaultNetworkBackupPath + "\logs\" + $defaultLogFileName;

# FUNCTIONS
function Get-WelcomeBanner {
	clear
    Write-Host "Welcome to the BEST. BACKUP. TOOL. EVER.";
    Write-Host "So let's backup ...";  
}

function Prepare-Copy {
    # check if every source has a destination
    if($sources.Count -ne $destinations.Count) { 
        thisIsTheEnd("Sources and destinations have a different amount of values");
    }
    # check if every destination is available, if not create it
    for($i=0;$i -lt $destinations.Count ; $i++){
        try {
            Set-Location $destinations[$i] -ErrorAction Stop | Out-Null;
        } catch {
            New-Item -ItemType Directory -Path $destinations[$i] | Out-Null;
        }   
    }
    # check if the local log file is available, if not create it
    if (-NOT (Test-Path $defaultLocalLogPath)) {
        New-Item -ItemType File -Force -Path $defaultLocalLogPath | Out-Null;
    }

    # check if the network backup folder is created, if not, create it
    if (-NOT (Test-Path $defaultNetworkBackupPath)) {
        New-Item -ItemType File -Force -Path $defaultNetworkBackupPath | Out-Null;
    }

    # check if the network log folder is created, if not create it
    $networkLogFolder = $defaultNetworkLogPath.Substring(0,$defaultNetworkLogPath.LastIndexOf("\"));
    if (-NOT (Test-Path $networkLogFolder)) {
        New-Item -ItemType Directory -Path $networkLogFolder | Out-Null;
    }

}

# ------------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------- SCRIPT START -----------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------

# WELCOME
# welcome user
Get-WelcomeBanner;

# COPY
# prepare copy
Prepare-Copy;

Write-Host "Back up in progress...";
# start copy
for($i=0;$i -lt $sources.Count ; $i++){
   robocopy $sources[$i] $destinations[$i] /MIR /E /XJ /MT /A-:SH /R:3 /W:3 /NP /XD '$RECYCLE.BIN' 'System Volume Information' /LOG+:$defaultLocalLogPath | Out-Null;
}
# copy log file to network location
Copy-Item -Path $defaultLocalLogPath -Destination $defaultNetworkLogPath | Out-Null;
# delete local log folder
Remove-Item -Path $defaultLocalLogPath.Substring(0, $defaultLocalLogPath.LastIndexOf("\")) -Force -Recurse | Out-Null;

Write-Host "Backup for" $defaultBackupFolderName "done!";

exit;
# ------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------ SCRIPT END ------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------