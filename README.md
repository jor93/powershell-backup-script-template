# Windows Backup Tool

Windows Backup Tool to backup all of your data to a network share. This readme handles following topcis:

  - **How to use it.** *Explination of how to use the script.*
  - **How to use set it up.** *Explination of how to set it up for the first time (new client).*

# How to use it!
1. Simply right click --> Run with Powershell
2. Wait
3. Done!

# How to set it up!
1. Create a new copy of 'TEMPLATE_Windows_Backup_Script.ps1' and rename it
2. Open the file and replace every 'XXX' by your needs
3. Add new sources and destinations if needed
4. If you want an automated backup create a new scheduled task
5. Done!


   [git-lab]: <https://gitlab.com/jor93>
   [windows-backup-script-template]: <https://gitlab.com/jor93/powershell-backup-script-template>
  
*made with ♥ by jor*